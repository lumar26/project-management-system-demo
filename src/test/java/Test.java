import domain.user.User;
import repository.user_repo.UserTable;
import util.properties.PropertiesCache;

import java.sql.*;

public class Test {
    public static void main(String[] args) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection connection = DriverManager.getConnection(PropertiesCache.getInstance().getProperty("database_url"),
                    PropertiesCache.getInstance().getProperty("database_user"),
                    PropertiesCache.getInstance().getProperty("database_password"));
            UserTable userTable = new UserTable(connection);
            userTable.insertUser(new User("Lazar", "lamar", "lazar@gmail.com", "laza"));
            connection.close();
        } catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }
    }
}
