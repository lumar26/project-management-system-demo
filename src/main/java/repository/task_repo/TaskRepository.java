package repository.task_repo;

import domain.task.Task;

public interface TaskRepository  {
    void insertTask(Task task);
}
