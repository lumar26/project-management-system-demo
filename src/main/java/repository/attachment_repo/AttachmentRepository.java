package repository.attachment_repo;

import domain.attachment.Attachment;

public interface AttachmentRepository  {
    void insertTask(Attachment attachment);
}
