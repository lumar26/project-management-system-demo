package repository.user_repo;

import domain.user.User;

import java.sql.SQLException;

public interface UserRepository {
    public void insertUser(User user) throws SQLException;
}
