package repository.user_repo;

import domain.user.User;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

public class UserTable implements UserRepository{
    private final Connection connection;

    public UserTable(Connection connection) {
        this.connection = connection;
    }

    @Override
    public void insertUser(User user) throws SQLException {
        Statement s = connection.createStatement();
        String command = String.format("insert into " +
                "user (username, password, email, name)" +
                "values" +
                "('%s', '%s', '%s', '%s')",
                user.getUsername(), user.getPassword(), user.getEmail(), user.getName());
        s.executeUpdate(command);
    }
}
