package domain.attachment;

public enum AttachmentType {
    INPUT, OUTPUT
}
