package domain.attachment;

import java.nio.file.Path;
import java.time.LocalDate;

public class Attachment {
    private final String name;
    private final Path path;
    private final LocalDate uploadDate;
    private final String filename;
    private final String extension;
    private final AttachmentType type;

    public Attachment(String name, Path path, String filename, String extension, AttachmentType type) {
        this.name = name;
        this.path = path;
        this.filename = filename;
        this.extension = extension;
        this.type = type;
        this.uploadDate = LocalDate.now();
    }

    public String getName() {
        return name;
    }

    public Path getPath() {
        return path;
    }

    public LocalDate getUploadDate() {
        return uploadDate;
    }

    public String getFilename() {
        return filename;
    }

    public String getExtension() {
        return extension;
    }

    public AttachmentType getType() {
        return type;
    }
}
