package domain.role;

public enum TaskRole {
    DEVELOPER, TEAM_LEAD, PRODUCT_OWNER, WATCHER
}
