package domain.role;

import domain.task.Task;
import domain.user.User;

public class TaskUserRole {
    private final User user;
    private final Task task;
    private TaskRole role;

    public TaskUserRole(User user, Task task, TaskRole role) {
        this.user = user;
        this.task = task;
        this.role = role;
    }
}
