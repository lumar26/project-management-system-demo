package domain.task;

public enum TaskState {
    NEW, SCHEDULED, IN_PROGRESS, DONE, CANCELED, SUSPENDED
}
