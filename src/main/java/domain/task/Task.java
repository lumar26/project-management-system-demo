package domain.task;

import domain.attachment.Attachment;
import domain.user.User;

import java.time.LocalDate;
import java.util.List;

public class Task {
    private final String name;
    private final String description;
    private final LocalDate created;
    private final LocalDate startDate;
    private final LocalDate dueDate;
    private LocalDate finishedDate;
    private TaskState currentSstate;

    private List<User> members;
    private List<User> watchers;
    private User responsible;

    private List<Attachment> inputAttachments;
    private List<Attachment> outputAttachments;


    public Task(String name, String description, LocalDate created, LocalDate startDate, LocalDate dueDate, List<User> members, List<User> watchers, User responsible) {
        this.name = name;
        this.description = description;
        this.created = created;
        this.startDate = startDate;
        this.dueDate = dueDate;
        this.members = members;
        this.watchers = watchers;
        this.responsible = responsible;
        this.currentSstate = TaskState.NEW;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public LocalDate getCreated() {
        return created;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public LocalDate getFinishedDate() {
        return finishedDate;
    }

    public void setFinishedDate(LocalDate finishedDate) {
        this.finishedDate = finishedDate;
    }

    public List<User> getMembers() {
        return members;
    }

    public void setMembers(List<User> members) {
        this.members = members;
    }

    public List<User> getWatchers() {
        return watchers;
    }

    public void setWatchers(List<User> watchers) {
        this.watchers = watchers;
    }

    public User getResponsible() {
        return responsible;
    }

    public void setResponsible(User responsible) {
        this.responsible = responsible;
    }
}
