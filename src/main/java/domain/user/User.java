package domain.user;

import domain.task.Task;

import java.util.List;

public class User {
    private String name;
    private final String username;
    private String email;
    private String password;

    private List<Task> participatingTasks;
    private List<Task> ownedTasks;
    private List<Task> supervisingTasks;

    public User(String name, String username, String email, String password) {
        this.name = name;
        this.username = username;
        this.email = email;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public List<Task> getParticipatingTasks() {
        return participatingTasks;
    }

    public List<Task> getOwnedTasks() {
        return ownedTasks;
    }

    public List<Task> getSupervisingTasks() {
        return supervisingTasks;
    }

    public void createTask(){

    }
}
